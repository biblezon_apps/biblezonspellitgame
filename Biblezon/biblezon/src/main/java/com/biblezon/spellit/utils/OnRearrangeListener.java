package com.biblezon.spellit.utils;

public interface OnRearrangeListener {
	
	public abstract void onRearrange(int oldIndex, int newIndex);
}
