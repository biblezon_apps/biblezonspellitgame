package com.biblezon.spellit.model;

/**
 * CatholicWords Model to store CatholicWords data like images and name into it.
 * 
 * @author Anshuman
 *
 */
public class CatholicWordsModel {
	/* Instance of words and images */
	String CatholicWordsText = "";
	Integer CatholicWordsImage = 0;

	/**
	 * get Word
	 * 
	 * @return
	 */
	public String getCatholicWordsText() {
		return CatholicWordsText;
	}

	/**
	 * Set Word
	 * 
	 * @param catholicWordsText
	 */
	public void setCatholicWordsText(String catholicWordsText) {
		CatholicWordsText = catholicWordsText;
	}

	/**
	 * Get Image
	 * 
	 * @return
	 */
	public Integer getCatholicWordsImage() {
		return CatholicWordsImage;
	}

	/**
	 * Set image
	 * 
	 * @param catholicWordsImage
	 */
	public void setCatholicWordsImage(Integer catholicWordsImage) {
		CatholicWordsImage = catholicWordsImage;
	}

}
