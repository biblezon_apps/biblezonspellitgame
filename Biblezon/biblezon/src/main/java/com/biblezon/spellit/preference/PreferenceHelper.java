package com.biblezon.spellit.preference;

/**
 * Preference Helper Class
 * 
 * @author Anshuman
 * 
 */
public interface PreferenceHelper {
	// Shared Preferences file name
	String PREFERENCE_NAME = "WORD_GAME_PREFERENCE";
	// Shared Preferences mode
	int PRIVATE_MODE = 0;
	// Name Match
	String PREFERENCE_SCORE_NAME_MATCH = "preference_score_name_match";
	// SPELL_IT
	String PREFERENCE_SCORE_SPELL_IT = "preference_score_spell_it";
	// TAP_IT
	String PREFERENCE_SCORE_TAP_IT = "preference_score_tap_it";
	// MEMORYMATCHGAME
	String PREFERENCE_SCORE_MEMORYMATCHGAME = "preference_score_memorymatchgame";

}
