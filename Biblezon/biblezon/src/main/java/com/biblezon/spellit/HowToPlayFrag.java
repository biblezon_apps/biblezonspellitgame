package com.biblezon.spellit;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class HowToPlayFrag extends Fragment {

	ListView lv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.howtoplay, container, false);
		lv = (ListView) view.findViewById(R.id.list_view);
		TextView tv = (TextView) view.findViewById(R.id.instructions);
		tv.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
				"comicbd.ttf"));

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		String[] instructions = getActivity().getResources().getStringArray(
				R.array.instruction);
		PlayAdapter adapter = new PlayAdapter(instructions);
		lv.setAdapter(adapter);
	}

	private class PlayAdapter extends BaseAdapter {
		String[] array;
		LayoutInflater infalter;

		public PlayAdapter(String[] array) {
			this.array = array;
			infalter = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			return array.length;
		}

		@Override
		public Object getItem(int pos) {
			return null;
		}

		@Override
		public long getItemId(int id) {
			return id;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View convertview, ViewGroup parent) {
			ViewHolder holder;
			if (convertview == null) {
				holder = new ViewHolder();
				convertview = infalter.inflate(R.layout.list_item, null);
				holder.txtView = (TextView) convertview
						.findViewById(R.id.instructions);
				convertview.setTag(holder);
			} else {
				holder = (ViewHolder) convertview.getTag();
			}

			holder.txtView.setText(array[position]);
			holder.txtView.setTypeface(Typeface.createFromAsset(getActivity()
					.getAssets(), "comicbd.ttf"));

			return convertview;
		}

		public class ViewHolder {
			TextView txtView;
		}

	}
}
