package com.biblezon.spellit.utils;

public interface OnMovementListener {
	public abstract void onMovement();
}
