package com.biblezon.spellit.utils;

import java.util.ArrayList;
import java.util.HashMap;

import com.biblezon.spellit.GameData;

public class NameList {

	public static ArrayList<String> getGameList(String whichGame) {
		if (whichGame.equalsIgnoreCase("game1"))
			return getGame1List();
		// return dummyGameList(); //for testing purpose only
		else
			return getGame2List();
	}

	public static ArrayList<String> getGame1List() {

		ArrayList<String> game1List = new ArrayList<String>();

		game1List.add("Genesis");
		game1List.add("Exodus");
		game1List.add("Leviticus");
		game1List.add("Numbers");
		game1List.add("Deuteronomy");
		game1List.add("Joshua");
		game1List.add("Judges");
		game1List.add("Ruth");
		game1List.add("1 Samuel");
		game1List.add("2 Samuel");
		game1List.add("1 Kings");
		game1List.add("2 Kings");
		game1List.add("1 Chronicles");
		game1List.add("2 Chronicles");
		game1List.add("Ezra");
		game1List.add("Nehemiah");

		game1List.add("Tobit");
		game1List.add("Judith");
		game1List.add("Easther");
		game1List.add("1 Maccabees");
		game1List.add("2 Maccabees");
		game1List.add("Job");
		game1List.add("Psalms");
		game1List.add("Proverbs");
		game1List.add("Ecclesiastes");
		game1List.add("Song of Songs");
		game1List.add("Wisdom");
		game1List.add("Sirach");
		game1List.add("Isaiah");
		game1List.add("Jeremiah");
		game1List.add("Lamentations");
		game1List.add("Baruch");

		game1List.add("Ezekiel");
		game1List.add("Daniel");
		game1List.add("Hosea");
		game1List.add("Amos");
		game1List.add("Obadiah");
		game1List.add("Jonah");
		game1List.add("Micah");
		game1List.add("Nahum");
		game1List.add("Habakkuk");
		game1List.add("Zephaniah");
		game1List.add("Haggai");
		game1List.add("Zechriah");
		game1List.add("Malachi");

		return game1List;
	}

	public static ArrayList<String> getGame2List() {

		ArrayList<String> game2List = new ArrayList<String>();

		game2List.add("Matthew");
		game2List.add("Mark");
		game2List.add("Luke");
		game2List.add("John");
		game2List.add("Acts");
		game2List.add("Romans");
		game2List.add("1 Corinthians");
		game2List.add("2 Corinthians");
		game2List.add("Galatians");

		game2List.add("Ephesians");
		game2List.add("Philippians");
		game2List.add("Colossians");
		game2List.add("1 Thessaloniars");
		game2List.add("2 Thessaloniars");
		game2List.add("1 Timothy");
		game2List.add("2 Timothy");
		game2List.add("Titus");
		game2List.add("Philemon");

		game2List.add("Hebrews");
		game2List.add("James");
		game2List.add("1 Peter");
		game2List.add("2 Peter");
		game2List.add("1 John");
		game2List.add("2 John");
		game2List.add("3 John");
		game2List.add("Jude");
		game2List.add("Revelation");

		return game2List;

	}

	// for testing purpose only
	public static ArrayList<String> dummyGameList() {

		ArrayList<String> dummyGameList = new ArrayList<String>();
		dummyGameList.add("A");
		dummyGameList.add("B");
		dummyGameList.add("C");
		dummyGameList.add("D");
		dummyGameList.add("E");

		return dummyGameList;
	}

	public static HashMap<String, GameData> generateGameData(String whichGame) {

		HashMap<String, GameData> data = new HashMap<String, GameData>();
		ArrayList<String> gameList = null;
		if (whichGame.equals("game1")) {
			gameList = getGame1List();
			// gameList = dummyGameList();
		} else {
			gameList = getGame2List();
		}

		for (int i = 0; i < gameList.size(); i++) {
			GameData gameData = new GameData(i + 1, gameList.get(i));
			data.put(gameList.get(i), gameData);
		}

		return data;
	}
}
