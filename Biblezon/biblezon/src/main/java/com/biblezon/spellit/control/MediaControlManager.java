package com.biblezon.spellit.control;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

public class MediaControlManager {
	public static MediaControlManager mMediaControl;
	static Activity activity;
	MediaPlayer player;

	public static MediaControlManager getInstance(Activity mActivity) {
		try {
			activity = mActivity;
			if (mMediaControl == null) {
				mMediaControl = new MediaControlManager();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return mMediaControl;
	}

	@SuppressLint("DefaultLocale")
	public void startPlay(String raw_file) {
		raw_file = raw_file.replaceAll(" ", "").toLowerCase();
		try {
			int resID = activity.getResources().getIdentifier(raw_file, "raw",
					activity.getPackageName());
			player = MediaPlayer.create(activity, resID);
			player.start();
			PlaySound.getInstance(activity).pauseSound();
			player.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					PlaySound.getInstance(activity).resumeSound();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void startPlay(int raw_file) {
		try {
			player = MediaPlayer.create(activity, raw_file);
			player.start();
			PlaySound.getInstance(activity).pauseSound();
			player.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					PlaySound.getInstance(activity).resumeSound();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopPlay() {
		try {
			if (player != null) {
				player.stop();
				player.release();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
