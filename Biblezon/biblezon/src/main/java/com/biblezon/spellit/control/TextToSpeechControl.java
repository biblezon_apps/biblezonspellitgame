package com.biblezon.spellit.control;

import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.util.Log;

import com.biblezon.spellit.utils.AppDefaultUtils;

@SuppressWarnings("deprecation")
public class TextToSpeechControl implements TextToSpeech.OnInitListener,
		OnUtteranceCompletedListener {
	/** Called when the activity is first created. */

	private TextToSpeech tts;
	Activity mActivity;
	String TAG = TextToSpeechControl.class.getSimpleName();
	String word = "";

	public TextToSpeechControl(Activity mActivity) {
		this.mActivity = mActivity;
		tts = new TextToSpeech(mActivity, this);
	}

	public void startTTS(String word) {
		stopTTS();
		speakOut();
	}

	public void stopTTS() {
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
	}

	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			tts.setOnUtteranceCompletedListener(this);
			int result = tts.setLanguage(Locale.US);
			tts.setPitch(2);
			tts.setSpeechRate(0.4f);
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "Language is not supported");
			} else {
				// speakOut();
			}
		} else {
			Log.e("TTS", "Initilization Failed");
		}
	}

	private void speakOut() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "theUtId");
		tts.speak(word, TextToSpeech.QUEUE_FLUSH, params);
	}

	@Override
	public void onUtteranceCompleted(String utteranceId) {
		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				AppDefaultUtils.showLog(TAG, "onUtteranceCompleted");
			}
		});

	}
}
